import csv
import sqlite3
import sys

from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QColor, QSyntaxHighlighter, QTextCharFormat, QFont
from PyQt5.QtWidgets import QWidget, QAction, QApplication, QPushButton, QHBoxLayout, QVBoxLayout, QListWidget, \
    QPlainTextEdit, QMainWindow, QFileDialog, QLineEdit

con = sqlite3.connect('base.sqlite')
cur = con.cursor()

# creating snippets table
cur.execute("""CREATE TABLE IF NOT EXISTS snippets (
	id integer PRIMARY KEY,
	name text,
	`text` text,
	folder_id integer
);""")

# creating folders table
cur.execute("""CREATE TABLE IF NOT EXISTS folders (
	id integer PRIMARY KEY,
	name text
);""")

cur.execute('SELECT f.id FROM folders as f WHERE f.name = "Default"')
if not cur.fetchall:
	cur.execute('INSERT INTO folders (name) VALUES ("Default")')  # initialize start folder
	con.commit()

cur.execute('SELECT f.name FROM folders as f')
folders: [str] = [row[0] for row in cur.fetchall()]  # initialize folders for QListWidget

snippets = {}


def format(color, style=''):
	_color = QColor()
	if type(color) is not str:
		_color.setRgb(color[0], color[1], color[2])
	else:
		_color.setNamedColor(color)

	_format = QTextCharFormat()
	_format.setForeground(_color)
	if 'bold' in style:
		_format.setFontWeight(QFont.Bold)
	if 'italic' in style:
		_format.setFontItalic(True)

	return _format

# styles for PythonHighlighter
STYLES = {
	'keyword': format([200, 120, 50], 'bold'),
	'operator': format([150, 150, 150]),
	'brace': format('darkGray'),
	'defclass': format([220, 220, 255], 'bold'),
	'string': format([20, 110, 100]),
	'string2': format([30, 120, 110]),
	'comment': format([128, 128, 128]),
	'self': format([150, 85, 140], 'italic'),
	'numbers': format([100, 150, 190]),
	'sql': format([143, 0, 255], 'bold')
}


class PythonHighlighter(QSyntaxHighlighter):
	keywords = [
		'and', 'assert', 'break', 'class', 'continue', 'def',
		'del', 'elif', 'else', 'except', 'exec', 'finally',
		'for', 'from', 'global', 'if', 'import', 'in',
		'is', 'lambda', 'not', 'or', 'pass', 'print',
		'raise', 'return', 'try', 'while', 'yield',
		'None', 'True', 'False',
	]

	operators = [
		'=',
		# Comparison
		'==', '!=', '<', '<=', '>', '>=',
		# Arithmetic
		'\+', '-', '\*', '/', '//', '\%', '\*\*',
		# In-place
		'\+=', '-=', '\*=', '/=', '\%=',
		# Bitwise
		'\^', '\|', '\&', '\~', '>>', '<<',
	]

	braces = [
		'\{', '\}', '\(', '\)', '\[', '\]',
	]

	sql_r = [
		'SELECT', 'INSERT', 'UPDATE', 'DELETE',
		'FROM', 'AS', 'INTO', 'WHERE', 'IN'
	]

	sql = [*sql_r, *[word.lower() for word in sql_r]]

	def __init__(self, document):
		QSyntaxHighlighter.__init__(self, document)

		self.tri_single = (QRegExp("'''"), 1, STYLES['string2'])
		self.tri_double = (QRegExp('"""'), 2, STYLES['string2'])

		rules = []

		rules += [(r'\b%s\b' % sql, 0, STYLES['sql'])
		          for sql in PythonHighlighter.sql]
		rules += [(r'\b%s\b' % w, 0, STYLES['keyword'])
		          for w in PythonHighlighter.keywords]
		rules += [(r'%s' % o, 0, STYLES['operator'])
		          for o in PythonHighlighter.operators]
		rules += [(r'%s' % b, 0, STYLES['brace'])
		          for b in PythonHighlighter.braces]

		rules += [
			(r'\bself\b', 0, STYLES['self']),

			(r'"[^"\\]*(\\.[^"\\]*)*"', 0, STYLES['string']),
			(r"'[^'\\]*(\\.[^'\\]*)*'", 0, STYLES['string']),

			(r'\bdef\b\s*(\w+)', 1, STYLES['defclass']),
			(r'\bclass\b\s*(\w+)', 1, STYLES['defclass']),

			(r'#[^\n]*', 0, STYLES['comment']),

			(r'\b[+-]?[0-9]+[lL]?\b', 0, STYLES['numbers']),
			(r'\b[+-]?0[xX][0-9A-Fa-f]+[lL]?\b', 0, STYLES['numbers']),
			(r'\b[+-]?[0-9]+(?:\.[0-9]+)?(?:[eE][+-]?[0-9]+)?\b', 0, STYLES['numbers']),
		]

		self.rules = [(QRegExp(pat), index, fmt)
		              for (pat, index, fmt) in rules]

	def highlightBlock(self, text):
		for expression, nth, format in self.rules:
			index = expression.indexIn(text, 0)

			while index >= 0:
				index = expression.pos(nth)
				length = len(expression.cap(nth))
				self.setFormat(index, length, format)
				index = expression.indexIn(text, index + length)

		self.setCurrentBlockState(0)

		in_multiline = self.match_multiline(text, *self.tri_single)
		if not in_multiline:
			in_multiline = self.match_multiline(text, *self.tri_double)

	def match_multiline(self, text, delimiter, in_state, style):
		if self.previousBlockState() == in_state:
			start = 0
			add = 0

		else:
			start = delimiter.indexIn(text)
			add = delimiter.matchedLength()

		while start >= 0:
			end = delimiter.indexIn(text, start + add)
			if end >= add:
				length = end - start + add + delimiter.matchedLength()
				self.setCurrentBlockState(0)
			else:
				self.setCurrentBlockState(in_state)
				length = len(text) - start + add
			self.setFormat(start, length, style)
			start = delimiter.indexIn(text, start + length)

		if self.currentBlockState() == in_state:
			return True
		else:
			return False


class MainWindow(QMainWindow):
	def __init__(self):
		super().__init__()
		self.initUI()

	def initUI(self):
		self.setCentralWidget(QWidget(self))
		self.folder_state: str = 'Default'
		self.snippet_state: str = ''
		self.main_layout = QHBoxLayout()
		self.centralWidget().setLayout(self.main_layout)
		self.setStyleSheet("""
            QLineEdit {
                border: none;
                border-bottom: 1px solid black;
                background: none;
            }
            
            QPushButton {
                background:none
            }
        """)
		self.menubar_init()
		self.folder_list_ui()
		self.snippets_list_ui()
		self.text_editor_ui()

	def menubar_init(self):
		# <menubar>
		self.menubar = self.menuBar()
		self.filemenu = self.menubar.addMenu('File')
		self.add_folder_action = QAction('Add folder', self)
		self.add_import_action = QAction('Import snippets', self)
		self.add_import_action.triggered.connect(self.import_snippets)
		self.add_export_action = QAction('Export snippets', self)
		self.add_export_action.triggered.connect(self.export_snippets)
		self.filemenu.addAction(self.add_folder_action)
		self.filemenu.addAction(self.add_import_action)
		self.filemenu.addAction(self.add_export_action)

	# </ menubar>

	# <folder_list>
	def folder_list_ui(self):
		self.folders_layout = QVBoxLayout()
		self.folders_add_lay = QHBoxLayout()
		# <folder_input>
		self.folders_input = QLineEdit()
		self.folders_add_lay.addWidget(self.folders_input)
		# </ folder_input>
		# <folder_button>
		self.folders_btn = QPushButton('Add folder')
		self.folders_btn.clicked.connect(self.new_folder)
		self.folders_add_lay.addWidget(self.folders_btn)
		self.folders_layout.addLayout(self.folders_add_lay)
		# </ snippet_button>
		self.folder_list = QListWidget()
		self.folders_layout.addWidget(self.folder_list)
		self.main_layout.addLayout(self.folders_layout)
		self.folder_list.addItems(folders)
		self.folder_list.itemClicked.connect(self.change_folder)

	# </ folder_list>

	def snippets_list_ui(self):
		# <snippets_list>
		self.snippets_layout = QVBoxLayout()
		self.snippets_add_lay = QHBoxLayout()
		# <snippet_input>
		self.snippets_input = QLineEdit()
		self.snippets_add_lay.addWidget(self.snippets_input)
		# <snippet_button>
		self.snippet_btn = QPushButton('Add snippet')
		self.snippet_btn.clicked.connect(self.new_snippet)
		self.snippets_add_lay.addWidget(self.snippet_btn)
		self.snippets_layout.addLayout(self.snippets_add_lay)
		# </ snippet_button>

		self.snippets_list = QListWidget()
		self.snippets_layout.addWidget(self.snippets_list)
		self.snippets_list.itemClicked.connect(self.change_snippet)
		self.main_layout.addLayout(self.snippets_layout)
		cur.execute(
			f"SELECT s.name FROM snippets as s WHERE s.folder_id IN (SELECT f.id FROM folders as f WHERE f.name = '{self.folder_state}')")
		self.snippets_list.addItems([row[0] for row in cur.fetchall()])

	# </ snippets_list>

	def text_editor_ui(self):
		# <text_editor>
		self.editor_lay = QVBoxLayout()
		self.editor_change_lay = QHBoxLayout()
		# <snippet_input>
		self.editor_input = QLineEdit()
		self.editor_change_lay.addWidget(self.editor_input)
		# <editor_btn>
		self.editor_btn = QPushButton('Change snippet name')
		self.editor_btn.clicked.connect(self.change_snippet_name)
		self.editor_change_lay.addWidget(self.editor_btn)
		# </ editor_btn>
		self.editor_lay.addLayout(self.editor_change_lay)
		self.text_editor = QPlainTextEdit()
		self.text_editor.textChanged.connect(self.changed_text)
		self.text_editor.setTabStopWidth(12)
		self.highlight = PythonHighlighter(self.text_editor.document())
		self.editor_lay.addWidget(self.text_editor)
		self.main_layout.addLayout(self.editor_lay)

	# </ text_editor>

	def change_folder(self, item):
		try:
			self.folder_state = item.text()
			self.snippets_list.clear()
			cur.execute(
				f'SELECT s.name FROM snippets as s WHERE s.folder_id IN (SELECT f.id from folders as f WHERE f.name = "{self.folder_state}")')
			res = cur.fetchall()
			self.snippets_list.addItems([row[0] for row in res])
		except Exception as e:
			pass

	def change_snippet(self, item):
		try:
			if self.snippet_state != item.text():
				text = self.text_editor.toPlainText()
				cur.execute(f'UPDATE snippets SET text = "{text}" WHERE name = "{self.snippet_state}"')
				con.commit()
				self.text_editor.clear()
				cur.execute(f'SELECT s.text FROM snippets as s WHERE s.name = "{item.text()}"')
				res = cur.fetchone()
				self.text_editor.setPlainText(res[0])
				self.snippet_state = item.text()
				self.editor_input.setText(self.snippet_state)
				self.editor_input.setText(self.snippet_state)
		except Exception as e:
			pass

	def change_snippet_name(self):
		try:
			old_name = self.snippet_state
			new_name = self.editor_input.text()
			cur.execute(f'UPDATE snippets SET name = "{new_name}" WHERE name = "{old_name}"')
			con.commit()
			self.snippet_state = new_name
			self.snippets_list.clear()
			cur.execute(
				f'SELECT s.name FROM snippets as s WHERE s.folder_id IN (SELECT f.id FROM folders as f WHERE f.name = "{self.folder_state}")')
			self.snippets_list.addItems([row[0] for row in cur.fetchall()])
		except Exception as e:
			pass

	def changed_text(self):
		try:
			text = self.text_editor.toPlainText()
			cursor = self.text_editor.textCursor()
			if self.text_editor.toPlainText()[cursor.position() - 1] == '(':
				self.text_editor.insertPlainText(')')
			elif self.text_editor.toPlainText()[cursor.position() - 1] == '{':
				self.text_editor.insertPlainText('}')
			elif self.text_editor.toPlainText()[cursor.position() - 1] == '[':
				self.text_editor.insertPlainText(']')
			elif self.text_editor.toPlainText()[cursor.position() - 1] == '\'':
				self.text_editor.insertPlainText('\'')
			elif self.text_editor.toPlainText()[cursor.position() - 1] == '\"':
				self.text_editor.insertPlainText('\"')
		except Exception as e:
			pass

	def new_folder(self):
		try:
			name = self.folders_input.text()
			if name:
				cur.execute(
					f'INSERT INTO folders (name) VALUES ("{name}")')
				self.snippets_input.clear()
				con.commit()
				self.folder_list.addItem(name)
		except Exception as e:
			pass

	def new_snippet(self):
		try:
			name = self.snippets_input.text()
			if name:
				cur.execute(
					f'INSERT INTO snippets (name, folder_id) VALUES ("{name}", (SELECT f.id FROM folders as f WHERE f.name = "{self.folder_state}"))')
				self.snippets_input.clear()
				con.commit()
				self.snippets_list.addItem(name)
		except Exception as e:
			pass

	def export_snippets(self):
		try:
			fname = QFileDialog.getOpenFileName(self, 'Выбрать csv файл', 'new.csv')[0]
			cur.execute("SELECT s.name, s.text, f.name FROM snippets as s JOIN folders f on s.folder_id = f.id")
			snippets_csv = [[row_ for row_ in row] for row in cur.fetchall()]
			with open(fname, 'w', newline='') as csv_f:
				writer = csv.writer(csv_f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
				writer.writerows(snippets_csv)
		except Exception:
			pass
	# [snip_name; snip_text; folder] -> csv

	def import_snippets(self):
		try:
			fname = QFileDialog.getOpenFileName(self, 'Выбрать csv файл', 'new.csv')[0]
			with open(fname, 'r', newline='') as csv_f:
				reader = csv.reader(csv_f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
				for row in reader:
					cur.execute(f'SELECT f.id FROM folders as f WHERE f.name = "{row[2]}"')
					if not cur.fetchall():
						cur.execute(f'INSERT INTO folders (name) VALUES ("{row[2]}")')
						con.commit()
					cur.execute(f'SELECT s.id FROM snippets as s WHERE s.name = "{row[0]}" and s.text = "{row[1]}"')
					if not cur.fetchall():
						cur.execute(
							f'INSERT INTO snippets (name, text, folder_id) VALUES ("{row[0]}", "{row[1]}", (SELECT f.id FROM folders as f WHERE f.name = "{row[2]}"))')
						con.commit()
			cur.execute('SELECT f.name FROM folders as f')
			self.folder_list.clear()
			self.folder_list.addItems([row[0] for row in cur.fetchall()])
			self.snippets_list.clear()
			cur.execute(
				f'SELECT s.name FROM snippets as s WHERE s.folder_id IN (SELECT f.id from folders as f WHERE f.name = "{self.folder_state}")')
			self.snippets_list.addItems([row[0] for row in cur.fetchall()])
		except Exception as e:
			pass
	# [snip_name; snip_text; folder] -> csv


if __name__ == '__main__':
	app = QApplication(sys.argv)
	main = MainWindow()
	main.show()
	sys.exit(app.exec())
